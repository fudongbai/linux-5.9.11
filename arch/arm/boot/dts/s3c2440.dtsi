// SPDX-License-Identifier: GPL-2.0
/*
 * Samsung's S3C2440 family device tree source
 *
 * Copyright (c) 2021 Fudong Bai <fudongbai@gmail.com>
 */

#include <dt-bindings/clock/s3c2410.h>
#include "s3c24xx.dtsi"
#include "s3c2440-pinctrl.dtsi"

/ {
	model = "Samsung S3C2440 SoC";
	compatible = "samsung,s3c2440";

	clocks: clock-controller@4c000000 {
		compatible = "samsung,s3c2440-clock";
		reg = <0x4c000000 0x40>;
		#clock-cells = <1>;
	};

	timer@51000000 {
		clocks = <&clocks PCLK_PWM>;
		clock-names = "timers";
	};

	pinctrl@56000000 {
		compatible = "samsung,s3c2440-pinctrl";
	};

	uart0: serial@50000000 {
		compatible = "samsung,s3c2440-uart";
		clock-names = "uart", "clk_uart_baud2";
		clocks = <&clocks PCLK_UART0>, <&clocks PCLK_UART0>;
	};

	uart1: serial@50004000 {
		compatible = "samsung,s3c2440-uart";
		clock-names = "uart", "clk_uart_baud2";
		clocks = <&clocks PCLK_UART1>, <&clocks PCLK_UART2>;
	};

	uart2: serial@50008000 {
		compatible = "samsung,s3c2440-uart";
		clock-names = "uart", "clk_uart_baud2";
		clocks = <&clocks PCLK_UART1>, <&clocks PCLK_UART2>;
	};

	rtc: rtc@57000000 {
		clocks = <&clocks PCLK_RTC>;
		clock-names = "rtc";
	};

	i2c: i2c@54000000 {
		compatible = "samsung,s3c2440-i2c";
		clocks = <&clocks PCLK_I2C>;
		clock-names = "i2c";
	};

	mmc0: mmc@5a000000 {
		compatible = "samsung,s3c2440-sdi";
		reg = <0x5a000000 0x100>;
		interrupts = <0 0 21 3>;
		clocks = <&clocks PCLK_SDI>;
		clock-names = "sdi";
		status = "disabled";
	};

	udc0: udc@52000000 {
		compatible = "samsung,s3c2440-usbgadget";
		reg = <0x52000000 0x1000>;
		interrupts = <0 0 25 3>;
		clocks = <&clocks HCLK_USBD>;
		clock-names = "usb-device";
		status = "disabled";
	};

	nand0: nand@4e000000 {
		compatible = "samsung,s3c2440-nand";
		reg = <0x4e000000 0x100>;
		interrupts = <0 0 24 3>;
		clocks = <&clocks HCLK_NAND>;
		clock-names = "nand";
		status = "disabled";
	};
};
