// SPDX-License-Identifier: GPL-2.0
//
// Samsung's S3C24XX flattened device tree enabled machine
//
// Copyright (c) 2021 Fudong Bai < fudongbai@gmail.com>

#include <linux/irqchip.h>

#include <asm/mach/arch.h>

#include <plat/cpu.h>
#include <plat/pm.h>
#include <mach/map.h>

static void __init s3c2410_dt_map_io(void)
{
	s3c24xx_init_io(NULL, 0);
}

static void __init s3c2410_dt_init_machine(void)
{
	s3c_pm_init();
}

static const char *const s3c2410_dt_compat[] __initconst = {
	"samsung,s3c2410",
	"samsung,s3c2440",
	NULL
};

DT_MACHINE_START(S3C2410_DT, "Samsung s3c24x0 (Flattened Device Tree)")
	/* Maintainer: Fudong Bai < fudongbai@gmail.com> */
	.dt_compat		= s3c2410_dt_compat,
	.map_io			= s3c2410_dt_map_io,
	.init_machine	= s3c2410_dt_init_machine,
MACHINE_END
